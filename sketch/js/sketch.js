var size = 16;
var $h1 = $('h1');
var $canvas = $('#canvas');

function initialize_canvas(size) {
    $canvas.empty();
    for (var i = 0; i < size; ++i) {
	var $tr = $('<tr>');
	for (var j = 0; j < size; ++j) {
	    var $td = $('<td>');
	    $tr.append($td);
	}
	$canvas.append($tr);
    }
}

function reset_canvas() {
    $canvas.find('td').css({
	'background-color': 'inherit'
    });
}

initialize_canvas(size);


// color selection
var $colorInput = $('#color');
var color = $colorInput.val();
$colorInput.on('change', function() {
    color = $colorInput.val();
});


// draw
$canvas.on('mousemove', function(e) {
    if (e.target.tagName == 'TD') {
	$target = $(e.target);
	$target.css({
	    'background-color': color
	});
    }
});

// Let the controls appear
$controls = $('#controls');
$controls.hide()
$canvas.one('mouseover', function() {
    $controls.delay(400).fadeIn(1000);
});
    

// reset button
$reset = $('#reset');
$reset.on('click', function() {
    reset_canvas();
});



